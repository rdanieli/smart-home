import { User } from "./user";
import { db } from "../database/smartHomeDB";
import { Base } from "./base";
import { Cenario } from "./cenario";

export enum DeviceType {
    ON_OFF = 1
}

export class Device extends Base {
    public id : number;
    public key : string;
    public name : string;
    public cenarioId : number;
    public out: number;
    public type: number;
    public status: number;


    public save() {
        return db.transaction('rw', db.devices, async () => {
            this.id = await db.devices.put(this);
        });

    }
}