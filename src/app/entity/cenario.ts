import { Base } from "./base";
import { db } from "../database/smartHomeDB";
import { Device } from "./device";

export class Cenario extends Base {
    id: number;
    name: string;
    status: number;
    devices: Device[];

    public async loadNavigationProperties() {
        [this.devices] = await Promise.all([
            db.devices.where('cenarioId').equals(this.id).toArray()
        ]);
    }

    public save(){
        return db.transaction('rw', db.cenarios, async () => {
            let cenario = await db.cenarios.where('id').equals(this.id).first()
            
            if(!cenario){
                await db.cenarios.add(this)
            }
        });
    }
}