import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthServiceProvider } from './providers/auth-service/auth-service';
import { DeviceServiceProvider } from './providers/device-service/device-service';
import { HttpClientModule } from '@angular/common/http';
import { NewDevicePageModule } from './pages/new-device/new-device.module';
import { ConfigPageModule } from './pages/config/config.module';
import { IonicStorageModule } from '@ionic/storage';
import { MessageProvider } from './providers/message/message';
import { HomePageModule } from './pages/home/home.module';
import { LoginPageModule } from './pages/login/login.module';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NewDevicePageModule,
    ConfigPageModule,
    HomePageModule,
    LoginPageModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: 'smart_home',
         driverOrder: ['indexeddb', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthServiceProvider,
    DeviceServiceProvider,
    MessageProvider
  ]
})
export class AppModule {}
