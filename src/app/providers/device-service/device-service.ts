import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Device } from '../../entity/device';
import { db } from '../../database/smartHomeDB';
import { Cenario } from '../../entity/cenario';


/*
  Generated class for the DeviceServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DeviceServiceProvider {

  serverUrl: any;
  
  constructor(public http: HttpClient, public storage: Storage) {
    
  }
  
  ionViewDidLoad() {
    
  }

  public async getCenarios() : Promise<Cenario[]> {
    let cenarios = await db.cenarios.toArray();
    // Resolve array properties 'emails' and 'phones'
    // on each and every contact:
    await Promise.all (cenarios.map(cenario => cenario.loadNavigationProperties()));

    return cenarios;
  }

  refreshServerProperties(server: any): any {
    this.storage.set('server', server);

    this.serverUrl = 'http://'+ server.ip + '/' +  server.context;
    console.log(this.serverUrl);
  }

  newDevice(device: Device) {
    device.save();
  }

  public async getServer() {
    return await this.storage.get('server');
  }

  updateItem(device: Device) {
    this.storage.get('server').then((server) => {
      if(server){
        this.serverUrl = 'http://'+ server.ip + '/' +  server.context;
        if(device.type == 1){
          this.serverUrl += '?out=' + (device.out ? 1 : 0) + '&id=' + device.key;
        }

        let t = this.http.get(this.serverUrl);
        t.subscribe(data => {
          console.log('my data: ', data);
        })
      } 
    })
  }

  removeDevice(device: Device) {
    return db.devices.where('id').equals(device.id).delete()
  }
}
