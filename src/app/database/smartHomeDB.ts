import Dexie from 'dexie';
import { Device } from '../entity/device';
import { Cenario } from '../entity/cenario';

export class SmartHomeDB extends Dexie {
    public cenarios: Dexie.Table<Cenario, number>;
    public devices: Dexie.Table<Device, number>;
    
    constructor() {  
      super("SmartHomeDB");
      
      let db = this

      //
      // Define tables and indexes
      // (Here's where the implicit table props are dynamically created)
      //
      db.version(1).stores({
        cenarios: '++id, name, status' + this.getCtrlProps(),
        devices: '++id, name, cenarioId, out, type, status' + this.getCtrlProps(),
      });

      db.devices.mapToClass(Device);
      db.cenarios.mapToClass(Cenario);
    }

    private getCtrlProps() {
      return ', dateInsert, dateUpdate, userNameInsert, userNameUpdate, ipInsert, ipUpdate';
    }
  }

export var db = new SmartHomeDB();