import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageProvider } from '../../providers/message/message';
import { DeviceServiceProvider } from '../../providers/device-service/device-service';
import { Device, DeviceType } from '../../entity/device';
import { Cenario } from '../../entity/cenario';

/**
 * Generated class for the NewDevicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-device',
  templateUrl: 'new-device.html',
})
export class NewDevicePage {
  public deviceForm: any;

  optCenarios = [
    { id:1, name:"Sala"},
    { id:2, name:"Cozinha"},
    { id:3, name:"Quarto"},
    { id:4, name:"Banheiro"}
  ];

  cenarioSelected;

  constructor(public viewCtrl: ViewController, public navParams: NavParams,
    formBuilder: FormBuilder, public message : MessageProvider, public deviceService : DeviceServiceProvider) {
      this.deviceForm = formBuilder.group({
        id: ['', Validators.required],
        cenario: ['', Validators.required],
        status:[''],
        nickname:['', Validators.required]

      });
  }

  ionViewDidLoad() {
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  public add() : void {
    let { id, cenario, status, nickname } = this.deviceForm.controls;

    if (!this.deviceForm.valid) {
      if (!id.valid) {
        this.message.showMessage( "Informe o ID do dispositivo");
        return
      }

      if (!cenario.valid) {
        this.message.showMessage("Informe o Cenário");
        return
      }

      if (!nickname.valid) {
        this.message.showMessage("Informe o Apelido");
        return
      }
    }
    else {
      let c = new Cenario();
      c.id = parseInt(this.cenarioSelected.id);
      c.name = this.cenarioSelected.name;
      c.save()

      let device = new Device();
      device.key = id.value;
      device.cenarioId = c.id;
      device.dateInsert = new Date;
      device.type=DeviceType.ON_OFF
      device.out=parseInt(status.value);
      device.name=nickname.value;

      this.deviceService.newDevice(device);
     
      if (this.viewCtrl) {
        try {
            this.viewCtrl.dismiss();
        }
        catch (exception) {

        }
        this.viewCtrl = null;
    }
      
    }
  }
}
