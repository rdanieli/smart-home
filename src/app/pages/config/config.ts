import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { DeviceServiceProvider } from '../../providers/device-service/device-service';
import { MessageProvider } from '../../providers/message/message';

/**
 * Generated class for the ConfigPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-config',
  templateUrl: 'config.html',
})
export class ConfigPage {

  public serverForm;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public toast: ToastController,
    public deviceService: DeviceServiceProvider,
    public message: MessageProvider
  ) {
    this.serverForm = formBuilder.group({
      ipservidor: ['', Validators.required],
      apiversion: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    let serverInfo = this.deviceService.getServer();
    serverInfo.then((value) => {
      if (value) {
        this.serverForm.controls.ipservidor.setValue(value.ip);
        this.serverForm.controls.apiversion.setValue(value.context);
      }
    });
  }

  

  refreshServerTrigger(): void {
    if (!this.serverForm.valid) {
      this.message.showMessage('Informe corretamente os dados do servidor.');
    }
    else {
      var server =
        {
          ip: this.serverForm.controls.ipservidor.value,
          context: this.serverForm.controls.apiversion.value

        }

      this.deviceService.refreshServerProperties(server);
      this.message.showMessage('Configurações Atualizadas.');
    }
  }

}
