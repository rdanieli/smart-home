import { Component } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';
import { DeviceServiceProvider } from '../../providers/device-service/device-service';
import { NewDevicePage } from '../new-device/new-device';
import { Device } from '../../entity/device';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  public cenarios;

  constructor(public nav: NavController, 
    private auth: AuthServiceProvider, 
    private deviceService: DeviceServiceProvider,
    private modalCtrl: ModalController,
    public alertCtrl: AlertController) {

  }

  public logout():void {
    this.auth.logout().subscribe(succ => {
      this.nav.setRoot(LoginPage)
    });
  }

  public addDevice():void {
    let myModal = this.modalCtrl.create(NewDevicePage);
    myModal.onDidDismiss(async () => {
      // this.deviceService.getCenarios().then((list) => {
      //   list.forEach(element => {
      //     element.loadNavigationProperties()
      //   });
      //   this.cenarios = list
      // });
      let cenarios = await this.deviceService.getCenarios();
      this.cenarios = cenarios

    });
    myModal.present();
  }

  ngOnInit() {
    this.load()
  }

  async load() {
    this.cenarios = await this.deviceService.getCenarios()
  }

  updateItem(device : Device) {
    this.deviceService.updateItem(device);      
  }

  public removeDevice(device : Device){
    this.showConfirm(device)
  }

  showConfirm(device : Device) {
    let confirm = this.alertCtrl.create({
      title: 'Deseja remover este Dispositivo?',
      message: 'Esta exclusão é permantente e não poderá ser recuperada no futuro.',
      buttons: [
        {
          text: 'Sim',
          handler: async () => {
            await this.deviceService.removeDevice(device)
            this.load()
          }
        },
        {
          text: 'Não',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }
}
